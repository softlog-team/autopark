package org.softlog.autopark.service;

import org.softlog.autopark.dao.GenericDao;
import org.softlog.autopark.model.Place;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PlaceService extends GenericService<Place, Integer> {
	@Autowired
	public PlaceService(GenericDao<Place, Integer> placeDao) {
		setDao(placeDao);
	}
}
