package org.softlog.autopark.service;

import java.util.Date;
import java.util.List;

import org.softlog.autopark.dao.ReportDao;
import org.softlog.autopark.model.report.DailyReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ReportService {
	@Autowired
	private ReportDao reportDao;

    public List<DailyReport> dailyReport(Date reportDate) {
        return reportDao.daily(reportDate);
    }
}
