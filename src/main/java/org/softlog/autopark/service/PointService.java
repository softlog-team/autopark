package org.softlog.autopark.service;

import java.util.List;

import org.softlog.autopark.dao.PointDao;
import org.softlog.autopark.model.Point;
import org.softlog.autopark.model.Race;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PointService extends GenericService<Point, Integer> {
	@Autowired
	public PointService(PointDao pointDao) {
		setDao(pointDao);
	}

	@Transactional
	public List<Point> findByRace(Race race) {
		return ((PointDao) getDao()).findByRace(race);
	}
}
