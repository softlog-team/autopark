package org.softlog.autopark.service;

import org.softlog.autopark.dao.GenericDao;
import org.softlog.autopark.model.CarType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CarTypeService extends GenericService<CarType, Integer> {
	@Autowired
	public CarTypeService(GenericDao<CarType, Integer> carTypeDao) {
		setDao(carTypeDao);
	}
}
