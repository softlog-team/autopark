package org.softlog.autopark.service;

import org.softlog.autopark.dao.GenericDao;
import org.softlog.autopark.model.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DriverService extends GenericService<Driver, Integer> {
	@Autowired
	public DriverService(GenericDao<Driver, Integer> driverDao) {
		setDao(driverDao);
	}
}
