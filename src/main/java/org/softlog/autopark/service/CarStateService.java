package org.softlog.autopark.service;

import org.softlog.autopark.dao.GenericDao;
import org.softlog.autopark.model.CarState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CarStateService extends GenericService<CarState, Integer> {
	@Autowired
	public CarStateService(GenericDao<CarState, Integer> carStateDao) {
		setDao(carStateDao);
	}
}
