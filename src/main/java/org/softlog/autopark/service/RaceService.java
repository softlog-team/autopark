package org.softlog.autopark.service;

import java.util.Date;
import java.util.List;

import org.softlog.autopark.dao.RaceDao;
import org.softlog.autopark.dao.ZoneDao;
import org.softlog.autopark.helper.Sorting;
import org.softlog.autopark.model.Race;
import org.softlog.autopark.model.Zone;
import org.softlog.autopark.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RaceService extends GenericService<Race, Integer> {
	@Autowired
	private ZoneDao zoneDao;

	@Autowired
	public RaceService(RaceDao raceDao) {
		setDao(raceDao);
	}

	public List<Race> findByDate(Date date, Sorting sort) {
		RaceDao raceDao = (RaceDao) getDao();
        return raceDao.findByDate(date, sort);
    }

	@Override
    public Race create(Race obj) {
    	RaceDao raceDao = (RaceDao) getDao();
    	if (obj.getDriver() != null)
    		raceDao.closeByDriver(obj.getDriver());
    	return raceDao.create(obj);
    }

	@Override
    public void update(Race obj) {
    	RaceDao raceDao = (RaceDao) getDao();

    	if (obj.getRoute() != null) {
	    	List<Zone> zones = zoneDao.findByRoute(obj.getRoute());
	    	if (zones.size() > 0)
	    		obj.setZone(zones.get(0));
    	}

    	if (obj.getDriver() != null)
    		raceDao.closeByDriver(obj.getDriver());

    	raceDao.update(obj);
    }

}
