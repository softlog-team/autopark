package org.softlog.autopark.service;

import org.softlog.autopark.dao.GenericDao;
import org.softlog.autopark.model.DriverCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DriverCategoryService extends GenericService<DriverCategory, Integer> {
	@Autowired
	public DriverCategoryService(GenericDao<DriverCategory, Integer> driverCategoryDao) {
		setDao(driverCategoryDao);
	}
}
