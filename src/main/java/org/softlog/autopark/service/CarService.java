package org.softlog.autopark.service;

import java.util.List;

import org.softlog.autopark.dao.GenericDao;
import org.softlog.autopark.dao.RaceDao;
import org.softlog.autopark.model.Car;
import org.softlog.autopark.model.Driver;
import org.softlog.autopark.model.Race;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CarService extends GenericService<Car, Integer> {
	@Autowired
	private RaceDao raceDao;

	@Autowired
	private GenericDao<Driver, Integer> driverDao;

	@Autowired
	public CarService(GenericDao<Car, Integer> carDao) {
		setDao(carDao);
	}

	public void delete(Integer id) {
		//driverDao.update("UPDATE Driver SET car = NULL WHERE car.id = ?1", new Object[] { id });
		//raceDao.update("UPDATE Race SET car = NULL WHERE car.id = ?1", new Object[] { id });

		getDao().delete(id);
	}
}
