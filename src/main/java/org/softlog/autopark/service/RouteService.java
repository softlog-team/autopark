package org.softlog.autopark.service;

import java.util.Iterator;
import java.util.List;

import org.softlog.autopark.dao.GenericDao;
import org.softlog.autopark.dao.RaceDao;
import org.softlog.autopark.dao.ZoneDao;
import org.softlog.autopark.model.Race;
import org.softlog.autopark.model.Route;
import org.softlog.autopark.model.Zone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RouteService extends GenericService<Route, Integer> {
	@Autowired
	private RaceDao raceDao;

	@Autowired
	private ZoneDao zoneDao;

	@Autowired
	public RouteService(GenericDao<Route, Integer> routeDao) {
		setDao(routeDao);
	}

	private Zone findZone(Route route) {
		List<Zone> zones = zoneDao.findByRoute(route);
		Zone result = null;

		if (zones.size() > 0)
			result = zones.get(0);

		return result;
	}

    public List<Route> list(String parameter, boolean ascending) {
        List<Route> routes = getDao().list(parameter, ascending);

        for (Iterator<Route> i = routes.iterator(); i.hasNext(); ) {
            Route item = i.next();
            item.setZone(findZone(item));
        }

        return routes;
    }

    public List<Route> list() {
        List<Route> routes = getDao().list();

        for (Iterator<Route> i = routes.iterator(); i.hasNext(); ) {
            Route item = i.next();
            item.setZone(findZone(item));
        }

        return routes;
    }

    public Route findOne(Integer id) {
    	Route route = getDao().find(id);
    	route.setZone(findZone(route));

    	return route;
    }

    public Route create(Route obj) {
    	Route result = getDao().create(obj);

    	Zone zone = new Zone();
    	zone.setRoute(obj);
    	zoneDao.create(zone);

    	return result;
    }

    public void update(Route obj) {
    	getDao().update(obj);

    	Zone newZone = obj.getZone();
    	if (newZone != null && newZone.getPolygon() != null
    			&& newZone.getPolygon().isValid()) {
    		List<Zone> zones = zoneDao.findByRoute(obj);

    		Boolean updated = false;
    		if (zones.size() > 0) {
    			Zone curZone = zones.get(0);
    			List<Race> races = raceDao.findByZone(curZone);

        		if (races.size() == 0) {
        			curZone.setPolygon(newZone.getPolygon());
        			zoneDao.update(curZone);
        			updated = true;
        		}
    		}

    		if (!updated) {
	    		newZone.setRoute(obj);
	    		zoneDao.create(newZone);
    		}
    	}
    }

    public void delete(Integer id) {
    	getDao().delete(id);
    }
}
