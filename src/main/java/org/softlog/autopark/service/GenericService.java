package org.softlog.autopark.service;

import java.io.Serializable;
import java.util.List;

import org.softlog.autopark.dao.GenericDao;
import org.softlog.autopark.helper.Sorting;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public abstract class GenericService<T, K extends Serializable> {
	private GenericDao<T, K> dao;

    protected GenericDao<T, K> getDao() {
    	return dao;
    }

	public void setDao(GenericDao<T, K> dao) {
		this.dao = dao;
	}

	public List<T> list(Sorting sort) {
		if (sort == null)
			return list();

		return dao.list(sort.getField(), sort.isAscending());
    }

    public List<T> list() {
        return dao.list();
    }

    public T findOne(K id) {
    	return dao.find(id);
    }

    public T create(T obj) {
    	return dao.create(obj);
    }

    public void update(T obj) {
    	dao.update(obj);
    }

    public void delete(K id) {
    	dao.delete(id);
    }
}
