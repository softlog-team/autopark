package org.softlog.autopark.service;

import org.softlog.autopark.dao.GenericDao;
import org.softlog.autopark.model.CarFuel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CarFuelService extends GenericService<CarFuel, Integer> {
	@Autowired
	public CarFuelService(GenericDao<CarFuel, Integer> carFuelDao) {
		setDao(carFuelDao);
	}
}
