package org.softlog.autopark.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDao <T, PK extends Serializable> {
    public T create(T obj);
    public void delete(PK id);
    public T find(PK id);
    public void update(T obj); 
    public List<T> list();
    public List<T> list(String property, boolean ascending);
}
