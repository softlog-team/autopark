package org.softlog.autopark.dao;

import java.util.List;

import org.softlog.autopark.model.Point;
import org.softlog.autopark.model.Race;

public interface PointDao extends GenericDao<Point, Integer> {
	List<Point> findByRace(Race race);
}
