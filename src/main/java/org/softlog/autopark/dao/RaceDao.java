package org.softlog.autopark.dao;

import java.util.Date;
import java.util.List;

import org.softlog.autopark.helper.Sorting;
import org.softlog.autopark.model.Driver;
import org.softlog.autopark.model.Race;
import org.softlog.autopark.model.Zone;

public interface RaceDao extends GenericDao<Race, Integer> {
	void closeByDriver(Driver driver);
	List<Race> findByZone(Zone zone);
	List<Race> findByDate(Date date, Sorting sort);
}
