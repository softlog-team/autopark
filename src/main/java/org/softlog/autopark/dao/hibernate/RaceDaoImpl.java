package org.softlog.autopark.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.softlog.autopark.dao.RaceDao;
import org.softlog.autopark.helper.Sorting;
import org.softlog.autopark.model.Driver;
import org.softlog.autopark.model.Race;
import org.softlog.autopark.model.Zone;
import org.springframework.stereotype.Repository;

@Repository
public class RaceDaoImpl extends HibernateDao<Race, Integer> implements RaceDao {
	@SuppressWarnings("unchecked")
	@Override
	public List<Race> findByDate(Date date, Sorting sort) {
		Criteria criteria = getSession().createCriteria(model);
		criteria.add(Restrictions.eq("execDate", date));

		if (sort != null) {
			if (sort.isAscending())
				criteria.addOrder(Order.asc(sort.getField()));
			else
				criteria.addOrder(Order.desc(sort.getField()));
		}

		List<Race> result = criteria.list();
		return result;
	}

	@Override
	public void closeByDriver(Driver driver) {
		update("Race.closeByDriver", new Object[] { driver });
	}

	@Override
	public List<Race> findByZone(Zone zone) {
		return list("Race.findByZone", new Object[] { zone });
	}
}
