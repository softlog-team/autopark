package org.softlog.autopark.dao.hibernate;

import java.util.List;

import org.softlog.autopark.dao.ZoneDao;
import org.softlog.autopark.model.Route;
import org.softlog.autopark.model.Zone;
import org.springframework.stereotype.Repository;

@Repository
public class ZoneDaoImpl extends HibernateDao<Zone, Integer> implements ZoneDao {
	@Override
	public List<Zone> findByRoute(Route route) {
		return list("Zone.findByRoute", new Object[] { route });
	}
}
