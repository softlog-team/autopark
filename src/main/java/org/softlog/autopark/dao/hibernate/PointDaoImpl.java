package org.softlog.autopark.dao.hibernate;

import java.util.List;

import org.softlog.autopark.dao.PointDao;
import org.softlog.autopark.model.Point;
import org.softlog.autopark.model.Race;
import org.springframework.stereotype.Repository;

@Repository
public class PointDaoImpl extends HibernateDao<Point, Integer> implements PointDao {
	@Override
	public List<Point> findByRace(Race race) {
		return list("Point.findByRace", new Object[] { race });
	}
}
