package org.softlog.autopark.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.softlog.autopark.dao.ReportDao;
import org.softlog.autopark.model.report.DailyReport;
import org.springframework.beans.factory.annotation.Autowired;

public class ReportDaoImpl implements ReportDao {
	@Autowired
    private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<DailyReport> daily(Date reportDate) {
		Query query = sessionFactory.getCurrentSession().
				createSQLQuery(
						"SELECT route, driver,"
						+ "execdate execDate, opentime openTime, closetime closeTime,"
						+ "car, mileage, mileage_outzone mileageOutZone, has_zone hasZone "
						+ "FROM auto_report_daily(:date)").
				addScalar("route").addScalar("driver").addScalar("execDate").
				addScalar("openTime").addScalar("closeTime").addScalar("car").
				addScalar("mileage").addScalar("mileageOutZone").addScalar("hasZone").
				setDate("date", reportDate).
				setResultTransformer(Transformers.aliasToBean(DailyReport.class));

		return query.list();
	}
}
