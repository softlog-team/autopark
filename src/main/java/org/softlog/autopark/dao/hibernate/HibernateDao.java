package org.softlog.autopark.dao.hibernate;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import org.softlog.autopark.dao.GenericDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class HibernateDao <T, PK extends Serializable> implements GenericDao <T, PK> {
	@Autowired
    private SessionFactory sessionFactory;

    protected Class<T> model;

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public Class<T> getModel() {
		return model;
	}

	public void setModel(Class<T> model) {
		this.model = model;
	}

	@Override
	public T create(T obj) {
		T result = obj;
		getSession().save(result); 
		return result;
	}

	@Override
	public void delete(PK id) {
		Object resource = getSession().load(model, id);
		if (resource != null)
			getSession().delete(resource);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T find(PK id) {
		T result;
		try {
			result = (T) getSession().get(model, id);
		} catch (ObjectNotFoundException e) {
			throw new IllegalArgumentException();
		}

		return result;
	}

	@Override
	public void update(T obj) {
		getSession().update(obj);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> list(String property, boolean ascending) {
		Criteria criteria = getSession().createCriteria(model);
		if (ascending)
			criteria.addOrder(Order.asc(property));
		else
			criteria.addOrder(Order.desc(property));

		List<T> result = criteria.list();

		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> list() {
		List<T> result = getSession().createCriteria(model).list();

		return result;
	}

	@SuppressWarnings("unchecked")
	public List<T> list(String namedQuery, Object[] arguments) {
		Query query = getSession().getNamedQuery(namedQuery);

		for (int i = 0; i < arguments.length; i++)
			query.setParameter(i, arguments[i]);

		return query.list();
	}

	public int update(String namedQuery, Object[] arguments) {
		Query query = getSession().getNamedQuery(namedQuery);

		for (int i = 0; i < arguments.length; i++)
			query.setParameter(i, arguments[i]);

		return query.executeUpdate();
	}
}
