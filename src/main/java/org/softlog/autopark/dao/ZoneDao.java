package org.softlog.autopark.dao;

import java.util.List;

import org.softlog.autopark.model.Route;
import org.softlog.autopark.model.Zone;

public interface ZoneDao extends GenericDao<Zone, Integer> {
	List<Zone> findByRoute(Route route);
}
