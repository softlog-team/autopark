package org.softlog.autopark.dao;

import java.util.Date;
import java.util.List;

import org.softlog.autopark.model.report.DailyReport;

public interface ReportDao {
	List<DailyReport> daily(Date reportDate);
}
