package org.softlog.autopark.helper;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class DateSerializer extends JsonSerializer<Date> {
	private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public Class<Date> handledType() {
		return Date.class;
	}

	@Override
	public void serialize(Date value, JsonGenerator gen, SerializerProvider arg2)
			throws IOException, JsonProcessingException {
		if (value instanceof Timestamp)
			gen.writeNumber(value.getTime());
		else
			gen.writeString(formatter.format(value));
	}
}
