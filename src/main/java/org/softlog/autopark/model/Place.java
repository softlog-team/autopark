package org.softlog.autopark.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the auto_places database table.
 * 
 */
@Entity
@Table(name="auto_places")
@NamedQuery(name="Place.findAll", query="SELECT p FROM Place p")
public class Place implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="aplace_id")
	private Integer id;

	@Column(name="aplace_name", length=50)
	private String name;

	public Place() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}