package org.softlog.autopark.model.report;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;

public class DailyReport implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="route")
	private String route;

	@Temporal(TemporalType.DATE)
	@Column(name="execdate")
	private Date execDate;

	@Column(name="opentime")
	private Timestamp openTime;

	@Column(name="closetime")
	private Timestamp closeTime;

	@Column(name="driver")
	private String driver;

	@Column(name="car")
	private String car;

	@Column(name="mileage")
	private Double mileage;

	@Column(name="mileage_outzone")
	private Double mileageOutZone;

	@Column(name="has_zone")
	private Boolean hasZone;

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public Date getExecDate() {
		return execDate;
	}

	public void setExecDate(Date execDate) {
		this.execDate = execDate;
	}

	public Timestamp getOpenTime() {
		return openTime;
	}

	public void setOpenTime(Timestamp openTime) {
		this.openTime = openTime;
	}

	public Timestamp getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(Timestamp closeTime) {
		this.closeTime = closeTime;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getCar() {
		return car;
	}

	public void setCar(String car) {
		this.car = car;
	}

	public Double getMileage() {
		return mileage;
	}

	public void setMileage(Double mileage) {
		this.mileage = mileage;
	}

	public Double getMileageOutZone() {
		return mileageOutZone;
	}

	public void setMileageOutZone(Double mileageOutZone) {
		this.mileageOutZone = mileageOutZone;
	}

	public Boolean getHasZone() {
		return hasZone;
	}

	public void setHasZone(Boolean hasZone) {
		this.hasZone = hasZone;
	}

}
