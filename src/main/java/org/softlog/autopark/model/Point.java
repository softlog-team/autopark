package org.softlog.autopark.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.TimeZone;

import javax.persistence.*;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entity implementation class for Entity: Point
 *
 */
@Entity
@Table(name="auto_points")
@Proxy(lazy=false)
@NamedQueries({
	@NamedQuery(name="Point.findAll", query="SELECT p FROM Point p"),
	@NamedQuery(name="Point.findByRace",
	           query="SELECT p FROM Point p, Race r " +
	                 "WHERE r = ? AND p.time >= r.openTime AND " +
	        		 "      (r.closeTime IS NULL OR p.time <= r.closeTime) AND " +
	        		 "      p.device = r.device " +
	        		 "ORDER BY p.time ASC")
})
public class Point implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="apoint_id")
	private Integer id;

	@ManyToOne
	@JoinColumn(name="apoint_device")
	@JsonIgnore
	private Device device;

	@Column(name="apoint_lat")
	private Float lat;

	@Column(name="apoint_lon")
	private Float lon;

	@Column(name="apoint_height")
	private Float height;

	@Column(name="apoint_dist")
	private Long distance;

	@Column(name="apoint_time")
	private Timestamp time;

	@Column(name="apoint_speed")
	private Float speed;

	@Column(name="apoint_dir")
	private Integer dir;

	@Column(name="apoint_charge")
	private Integer charge;

	@Column(name="apoint_systime")
	private Timestamp systime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Float getLat() {
		return lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public Float getLon() {
		return lon;
	}

	public void setLon(Float lon) {
		this.lon = lon;
	}

	public Float getHeight() {
		return height;
	}

	public void setHeight(Float height) {
		this.height = height;
	}

	public Long getDistance() {
		return distance;
	}

	public void setDistance(Long distance) {
		this.distance = distance;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public Float getSpeed() {
		return speed;
	}

	public void setSpeed(Float speed) {
		this.speed = speed;
	}

	public Integer getDir() {
		return dir;
	}

	public void setDir(Integer dir) {
		this.dir = dir;
	}

	public Integer getCharge() {
		return charge;
	}

	public void setCharge(Integer charge) {
		this.charge = charge;
	}

	public Timestamp getSystime() {
		return systime;
	}

	public void setSystime(Timestamp systime) {
		this.systime = systime;
	}
}
