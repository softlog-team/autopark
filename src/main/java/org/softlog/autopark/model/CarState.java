package org.softlog.autopark.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the auto_states database table.
 * 
 */
@Entity
@Table(name="auto_car_states")
@NamedQuery(name="CarState.findAll", query="SELECT s FROM CarState s")
public class CarState implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="acs_id")
	private Integer id;

	@Column(name="acs_name", length=25)
	private String name;

	public CarState() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}