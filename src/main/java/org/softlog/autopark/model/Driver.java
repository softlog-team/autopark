package org.softlog.autopark.model;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import java.util.Date;


/**
 * The persistent class for the auto_drivers database table.
 * 
 */
@Entity
@Table(name="auto_drivers")
@NamedQuery(name="Driver.findAll", query="SELECT d FROM Driver d")
public class Driver implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ad_id")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name="ad_license_date")
	private Date licenseDate;

	@Column(name="ad_license_num", length=8)
	private String licenseNum;

	@Column(name="ad_license_ser", length=6)
	private String licenseSer;

	@Column(name="ad_name", length=100)
	private String name;

	//bi-directional many-to-one association to AutoCar
	@ManyToOne
	@JoinColumn(name="ad_car")
	private Car car;

	//bi-directional many-to-one association to AutoCategory
	@ManyToOne
	@JoinColumn(name="ad_category")
	private DriverCategory category;

	public Driver() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getLicenseDate() {
		return this.licenseDate;
	}

	public void setLicenseDate(Date licenseDate) {
		this.licenseDate = licenseDate;
	}

	public String getLicenseNum() {
		return this.licenseNum;
	}

	public void setLicenseNum(String licenseNum) {
		this.licenseNum = licenseNum;
	}

	public String getLicenseSer() {
		return this.licenseSer;
	}

	public void setLicenseSer(String licenseSer) {
		this.licenseSer = licenseSer;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Car getCar() {
		return this.car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public DriverCategory getCategory() {
		return this.category;
	}

	public void setCategory(DriverCategory category) {
		this.category = category;
	}

	@PreRemove
	public void preRemove() {
		System.out.println("PRE_REMOVE2");
	    setCar(null);
	}
}