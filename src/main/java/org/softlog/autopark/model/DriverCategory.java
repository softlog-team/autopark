package org.softlog.autopark.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the auto_driver_class database table.
 * 
 */
@Entity
@Table(name="auto_driver_categories")
@NamedQuery(name="DriverCategory.findAll", query="SELECT d FROM DriverCategory d")
public class DriverCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="adc_id", unique=true, nullable=false)
	private Integer id;

	@Column(name="adc_name", length=3)
	private String name;

	public DriverCategory() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}