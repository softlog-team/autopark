package org.softlog.autopark.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the auto_fuel database table.
 * 
 */
@Entity
@Table(name="auto_car_fuel")
@NamedQuery(name="CarFuel.findAll", query="SELECT f FROM CarFuel f")
public class CarFuel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="acf_id", unique=true, nullable=false)
	private Integer id;

	@Column(name="acf_name", length=15)
	private String name;

	public CarFuel() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}