package org.softlog.autopark.model;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the auto_places database table.
 * 
 */
@Entity
@Table(name="auto_routes")
@NamedQuery(name="Route.findAll", query="SELECT r FROM Route r")
public class Route implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="aroute_id")
	private Integer id;

	@Column(name="aroute_name", length=50)
	private String name;

	@Transient
	private Zone zone;

	public Route() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

}