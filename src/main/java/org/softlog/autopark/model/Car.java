package org.softlog.autopark.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.softlog.autopark.helper.DateSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
/**
 * The persistent class for the auto_cars database table.
 * 
 */
@Entity
@Table(name="auto_cars")
@NamedQuery(name="Car.findAll", query="SELECT c FROM Car c")
public class Car implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ac_id", unique=true, nullable=false)
	private Integer id;

	@Column(name="ac_capacity")
	private Float capacity;

	@Temporal(TemporalType.DATE)
	@Column(name="ac_inspect_date")
	private Date inspectDate;

	@Column(name="ac_license", length=12)
	private String license;

	@Temporal(TemporalType.DATE)
	@Column(name="ac_manufactured")
	private Date manufactured;

	@Column(name="ac_mileage")
	private Long mileage;

	@Column(name="ac_model", length=50)
	private String model;

	@Temporal(TemporalType.DATE)
	@Column(name="as_purchased")
	private Date purchased;

	//bi-directional many-to-one association to CarType
	@ManyToOne
	@JoinColumn(name="ac_type")
	private CarType type;

	//bi-directional many-to-one association to AutoDevice
	@ManyToOne
	@JoinColumn(name="ac_device")
	private Device device;

	//bi-directional many-to-one association to AutoDriverCategory
	@ManyToOne
	@JoinColumn(name="ac_driver_class")
	private DriverCategory driverCategory;

	//bi-directional many-to-one association to Fuel
	@ManyToOne
	@JoinColumn(name="ac_fuel")
	private CarFuel fuel;

	//bi-directional many-to-one association to State
	@ManyToOne
	@JoinColumn(name="ac_state")
	private CarState state;

	public Car() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Float getCapacity() {
		return this.capacity;
	}

	public void setCapacity(Float capacity) {
		this.capacity = capacity;
	}

	public Date getInspectDate() {
		return this.inspectDate;
	}

	public void setInspectDate(Date inspectDate) {
		this.inspectDate = inspectDate;
	}

	public String getLicense() {
		return this.license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public Date getManufactured() {
		return this.manufactured;
	}

	public void setManufactured(Date manufactured) {
		this.manufactured = manufactured;
	}

	public Long getMileage() {
		return this.mileage;
	}

	public void setMileage(Long mileage) {
		this.mileage = mileage;
	}

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Date getPurchased() {
		return this.purchased;
	}

	public void setPurchased(Date purchased) {
		this.purchased = purchased;
	}

	public CarType getType() {
		return this.type;
	}

	public void setType(CarType type) {
		this.type = type;
	}

	public Device getDevice() {
		return this.device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public DriverCategory getDriverCategory() {
		return this.driverCategory;
	}

	public void setDriverCategory(DriverCategory driverCategory) {
		this.driverCategory = driverCategory;
	}

	public CarFuel getFuel() {
		return this.fuel;
	}

	public void setFuel(CarFuel fuel) {
		this.fuel = fuel;
	}

	public CarState getState() {
		return this.state;
	}

	public void setState(CarState state) {
		this.state = state;
	}
}