package org.softlog.autopark.model;

import java.io.Serializable;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.*;

import org.softlog.autopark.helper.DateSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * The persistent class for the auto_races database table.
 * 
 */
@Entity
@Table(name="auto_races")
@NamedQueries({
	@NamedQuery(name="Race.findAll", query="SELECT r FROM Race r"),
	@NamedQuery(name="Race.findByZone",
                query="SELECT r FROM Race r, Zone z WHERE r.zone = ?"),
    @NamedQuery(name="Race.closeByDriver",
                query="UPDATE Race r SET r.closeTime = CURRENT_TIMESTAMP " +
                      "WHERE r.driver = ?")
})
public class Race implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="arace_id")
	private Integer id;

	@ManyToOne
	@JoinColumn(name="arace_device")
	private Device device;

	@ManyToOne
	@JoinColumn(name="arace_car")
	private Car car;

	@ManyToOne
	@JoinColumn(name="arace_driver")
	private Driver driver;

	@ManyToOne
	@JoinColumn(name="arace_route")
	private Route route;

	@ManyToOne
	@JoinColumn(name="arace_zone")
	private Zone zone;

	@ManyToOne
	@JoinColumn(name="arace_place")
	private Place place;

	@Column(name="arace_tasktxt", length=2000)
	private String task;

	@Column(name="arace_closetime")
	private Timestamp closeTime;

	@Column(name="arace_opentime")
	private Timestamp openTime;

	@Temporal(TemporalType.DATE)
	@Column(name="arace_execdate", nullable=false)
	private Date execDate;

	@Column(name="arace_corr")
	private Double corr;

	@Column(name="arace_corrcause", length=2000)
	private String corrCause;

	public Race() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public Timestamp getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(Timestamp closeTime) {
		this.closeTime = closeTime;
	}

	public Timestamp getOpenTime() {
		return openTime;
	}

	public void setOpenTime(Timestamp startTime) {
		this.openTime = startTime;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getExecDate() {
		return execDate;
	}

	public void setExecDate(Date execDate) {
		this.execDate = execDate;
	}

	public Double getCorr() {
		return corr;
	}

	public void setCorr(Double corr) {
		this.corr = corr;
	}

	public String getCorrCause() {
		return corrCause;
	}

	public void setCorrCause(String corrCause) {
		this.corrCause = corrCause;
	}
}
