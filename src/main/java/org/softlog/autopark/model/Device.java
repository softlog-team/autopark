package org.softlog.autopark.model;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;
import java.math.BigDecimal;


/**
 * The persistent class for the auto_devices database table.
 * 
 */
@Entity
@Table(name="auto_devices")
@NamedQuery(name="Device.findAll", query="SELECT d FROM Device d")
public class Device implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="adev_id", unique=true, nullable=false)
	private Integer id;

	@Column(name="adev_code", length=15)
	private BigDecimal code;

	@Column(name="adev_last_config")
	private Timestamp lastConfig;

	@Column(name="adev_model", length=30)
	private String model;

	//bi-directional many-to-one association to DeviceType
	@ManyToOne
	@JoinColumn(name="adev_type")
	@JsonIgnoreProperties({"config", "configTime", "name" })
	private DeviceType deviceType;

	public Device() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getCode() {
		return this.code;
	}

	public void setCode(BigDecimal code) {
		this.code = code;
	}

	public Timestamp getLastConfig() {
		return this.lastConfig;
	}

	public void setLastConfig(Timestamp lastConfig) {
		this.lastConfig = lastConfig;
	}

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	/* Сериализуем как ID */
	public DeviceType getDeviceType() {
		return this.deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

}