package org.softlog.autopark.model;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vividsolutions.jts.geom.Polygon;
/**
 * The persistent class for the auto_places database table.
 * 
 */
@Entity
@Table(name="auto_zones")
@NamedQueries({
	@NamedQuery(name="Zone.findAll", query="SELECT z FROM Zone z"),
	@NamedQuery(name="Zone.findByRoute", query="SELECT z FROM Zone z WHERE z.route = ? ORDER BY z.id DESC")
})
public class Zone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="az_id")
	@JsonIgnore
	private Integer id;

	@ManyToOne
	@JoinColumn(name="az_route")
	@JsonIgnore
	private Route route;

	@Column(name="az_polygon")
	@Type(type="org.hibernate.spatial.GeometryType")
	private Polygon polygon;

	public Zone() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public Polygon getPolygon() {
		return polygon;
	}

	public void setPolygon(Polygon polygon) {
		this.polygon = polygon;
	}
}