package org.softlog.autopark.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the auto_car_types database table.
 * 
 */
@Entity
@Table(name="auto_car_types")
@NamedQuery(name="CarType.findAll", query="SELECT c FROM CarType c")
public class CarType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="act_id", unique=true, nullable=false)
	private Integer id;

	@Column(name="act_name", length=25)
	private String name;

	public CarType() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}