package org.softlog.autopark.model;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the auto_device_types database table.
 * 
 */
@Entity
@Table(name="auto_device_types")
@NamedQuery(name="DeviceType.findAll", query="SELECT d FROM DeviceType d")
public class DeviceType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="adt_id", unique=true, nullable=false)
	private Integer id;

	@Column(name="adt_config")
	@Lob
	@Type(type="org.hibernate.type.StringClobType")
	private String config;

	@Column(name="adt_config_time")
	private Timestamp configTime;

	@Column(name="adt_name", length=100)
	private String name;

	//bi-directional many-to-one association to Device
	@OneToMany(mappedBy="deviceType", cascade = CascadeType.REMOVE)
	@JsonIgnore
	private List<Device> devices;

	public DeviceType() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getConfig() {
		return this.config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public Timestamp getConfigTime() {
		return this.configTime;
	}

	public void setConfigTime(Timestamp configTime) {
		this.configTime = configTime;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Device> getDevices() {
		return this.devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	public Device addDevice(Device device) {
		getDevices().add(device);
		device.setDeviceType(this);

		return device;
	}

	public Device removeDevice(Device device) {
		getDevices().remove(device);
		device.setDeviceType(null);

		return device;
	}
}