package org.softlog.autopark.controller;

import org.softlog.autopark.model.Route;
import org.softlog.autopark.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/route")
public class RouteController extends GenericController<Route, Integer> {
	@Autowired
	public RouteController(RouteService routeService) {
		setService(routeService);
	}
}