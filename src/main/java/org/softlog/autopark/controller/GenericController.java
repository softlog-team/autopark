package org.softlog.autopark.controller;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.softlog.autopark.helper.Sorting;
import org.softlog.autopark.service.GenericService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public abstract class GenericController<T, PK extends Serializable> {
	private GenericService<T, PK> service;

	protected GenericService<T, PK> getService() {
		return service;
	}

	protected void setService(GenericService<T, PK> service) {
		this.service = service;
	}

	protected Sorting getSorting(HttpServletRequest request) {
		Pattern p = Pattern.compile("^sort\\(([\\+\\-\\s])(\\w+)\\)$");
		Set<?> parameters = request.getParameterMap().keySet();

		String column = "";
		boolean ascending = true;

		for (Object param : parameters) {
			Matcher m = p.matcher(param.toString());
			if (m.matches()) {
				column = m.group(2);
				ascending = m.group(1) == "+" || m.group(1).trim().isEmpty();
				break;
			}
		}

		if (column.isEmpty())
			return null;

		return new Sorting(column, ascending); 
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public Collection<T> list(HttpServletRequest request,
			HttpServletResponse response)
	{
		Collection<T> result;
		Sorting sort = getSorting(request);
		result = service.list(sort);

		int count = result.size();
		response.setHeader("Content-Range", "0-" + count + "/" + count);

		return result;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public T get(@PathVariable("id") PK id)
	{
		try {
			return service.findOne(id);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public T create(@RequestBody T resource) {
		return service.create(resource);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public T update(@RequestBody T resource) {
		service.update(resource);
		return resource;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable("id") PK id) {
		service.delete(id);
	}
}
