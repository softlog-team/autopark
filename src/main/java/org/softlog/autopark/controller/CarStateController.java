package org.softlog.autopark.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.softlog.autopark.model.CarState;
import org.softlog.autopark.service.CarStateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/car/state")
public class CarStateController {
	@Autowired
    private CarStateService carStateService;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<CarState> listCarStates(HttpServletRequest request,
			HttpServletResponse response)
	{
		List<CarState> result = carStateService.list();

		int count = result.size();
		response.setHeader("Content-Range", "0-" + count + "/" + count);

		return result;
	}
}
