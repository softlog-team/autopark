package org.softlog.autopark.controller;

import org.softlog.autopark.model.Driver;
import org.softlog.autopark.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/driver")
public class DriverController extends GenericController<Driver, Integer> {
	@Autowired
	public DriverController(DriverService driverService) {
		setService(driverService);
	}
}
