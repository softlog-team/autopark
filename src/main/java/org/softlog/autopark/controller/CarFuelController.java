package org.softlog.autopark.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.softlog.autopark.model.CarFuel;
import org.softlog.autopark.service.CarFuelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/car/fuel")
public class CarFuelController {
	@Autowired
    private CarFuelService carFuelService;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<CarFuel> listCarFuels(HttpServletRequest request,
			HttpServletResponse response)
	{
		List<CarFuel> result = carFuelService.list();

		int count = result.size();
		response.setHeader("Content-Range", "0-" + count + "/" + count);

		return result;
	}
}
