package org.softlog.autopark.controller;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.softlog.autopark.helper.Sorting;
import org.softlog.autopark.model.Car;
import org.softlog.autopark.model.Device;
import org.softlog.autopark.model.Driver;
import org.softlog.autopark.model.Point;
import org.softlog.autopark.model.Race;
import org.softlog.autopark.service.CarService;
import org.softlog.autopark.service.DriverService;
import org.softlog.autopark.service.PointService;
import org.softlog.autopark.service.RaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping("/race")
public class RaceController extends GenericController<Race, Integer> {
	@Autowired
	private DriverService driverService;

	@Autowired
	private CarService carService;

	@Autowired
	private PointService pointService;

	@Autowired
	public RaceController(RaceService raceService) {
		setService(raceService);
	}

	@RequestMapping(method = RequestMethod.GET, params="date")
	@ResponseBody
	public Collection<Race> list(@RequestParam("date") @DateTimeFormat(pattern="yyyy-MM-dd") Date date,
	                             HttpServletRequest request, HttpServletResponse response)
	{
		RaceService service = (RaceService) getService();

		Collection<Race> result;
		Sorting sort = getSorting(request);
		result = service.findByDate(date, sort);

		int count = result.size();
		response.setHeader("Content-Range", "0-" + count + "/" + count);

		return result;
	}

	protected Race preprocessRace(Race race) {
		RaceService service = (RaceService) getService();

		Race old = null;
		if (race.getId() != null)
			old = service.findOne(race.getId());

		Driver driver = race.getDriver();
		Car car = race.getCar();
		Device device = race.getDevice();

		if (driver != null && (old == null || (
				(old.getDriver() == null || driver.getId() != old.getDriver().getId()) &&
				(car == null || car.getId() == old.getCar().getId()) &&
				(device == null || device.getId() == old.getDevice().getId())
			))
		) {
			driver = driverService.findOne(driver.getId());
			car = driver.getCar();
			device = car.getDevice();

			race.setCar(car);
			race.setDevice(device);

			race.setOpenTime(new java.sql.Timestamp(new Date().getTime()));
		} else if (car != null && (old == null || (
				(old.getCar() == null || car.getId() != old.getCar().getId()) &&
				(device == null || device.getId() == old.getDevice().getId())
			))
		) {
			car = carService.findOne(car.getId());
			device = car.getDevice();

			race.setDevice(device);
		}

		if (race.getExecDate() == null)
			race.setExecDate(new java.sql.Date(new Date().getTime()));

		return race;
	}

	@Override
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public Race create(@RequestBody Race resource) {
		Race race = preprocessRace(resource);
		return getService().create(race);
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Race update(@RequestBody Race resource) {
		Race race = preprocessRace(resource);
		getService().update(race);
		return race;
	}

	@RequestMapping(value = "/track/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<Point> getTrack(@PathVariable("id") Integer id)
	{
		Race race = new Race();
		race.setId(id);

		List<Point> result = pointService.findByRace(race);

		return result;
	}
}