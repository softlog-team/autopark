package org.softlog.autopark.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.softlog.autopark.model.CarType;
import org.softlog.autopark.service.CarTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/car/type")
public class CarTypeController {
	@Autowired
    private CarTypeService carTypeService;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<CarType> listCarTypes(HttpServletRequest request,
			HttpServletResponse response)
	{
		List<CarType> result = carTypeService.list();

		int count = result.size();
		response.setHeader("Content-Range", "0-" + count + "/" + count);

		return result;
	}
}
