package org.softlog.autopark.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.softlog.autopark.model.DriverCategory;
import org.softlog.autopark.service.DriverCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/driver/category")
public class DriverCategoryController {
	@Autowired
    private DriverCategoryService driverCategoryService;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<DriverCategory> listDeviceTypes(HttpServletRequest request,
			HttpServletResponse response)
	{
		List<DriverCategory> result = driverCategoryService.list();

		int count = result.size();
		response.setHeader("Content-Range", "0-" + count + "/" + count);

		return result;
	}
}
