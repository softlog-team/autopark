package org.softlog.autopark.controller;

import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.softlog.autopark.model.report.*;
import org.softlog.autopark.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/report")
public class ReportController {
	@Autowired
	private ReportService service;

	@RequestMapping(value = "/daily", method = RequestMethod.GET)
	@ResponseBody
	public Collection<DailyReport> daily(@RequestParam("date") @DateTimeFormat(pattern="yyyy-MM-dd") Date date,
            HttpServletRequest request, HttpServletResponse response)
	{
		Collection<DailyReport> result = service.dailyReport(date);
		int count = result.size();

		response.setHeader("Content-Range", "0-" + count + "/" + count);

		return result;
	}
}
