package org.softlog.autopark.controller;

import org.softlog.autopark.model.Car;
import org.softlog.autopark.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/car")
public class CarController extends GenericController<Car, Integer> {
	@Autowired
	public CarController(CarService carService) {
		setService(carService);
	}
}
