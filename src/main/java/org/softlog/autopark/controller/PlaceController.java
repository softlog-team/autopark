package org.softlog.autopark.controller;

import org.softlog.autopark.model.Place;
import org.softlog.autopark.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/place")
public class PlaceController extends GenericController<Place, Integer> {
	@Autowired
	public PlaceController(PlaceService placeService) {
		setService(placeService);
	}
}