package org.softlog.autopark.controller;

import org.softlog.autopark.model.DeviceType;
import org.softlog.autopark.service.DeviceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/device/type")
public class DeviceTypeController extends GenericController<DeviceType, Integer> {
	@Autowired
	public DeviceTypeController(DeviceTypeService deviceTypeService) {
		setService(deviceTypeService);
	}
}