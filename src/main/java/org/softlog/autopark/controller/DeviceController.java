package org.softlog.autopark.controller;

import org.softlog.autopark.model.Device;
import org.softlog.autopark.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/device")
public class DeviceController extends GenericController<Device, Integer> {
	@Autowired
	public DeviceController(DeviceService deviceService) {
		setService(deviceService);
	}
}
