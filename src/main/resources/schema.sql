BEGIN;

-- auto_car_fuel --
CREATE TABLE IF NOT EXISTS auto_car_fuel
(
  acf_id   serial PRIMARY KEY,
  acf_name character varying(15)
);

comment on table  auto_car_fuel          is 'Типы топлива автопарка';
comment on column auto_car_fuel.acf_id   is 'Код типа топлива (ключ)';
comment on column auto_car_fuel.acf_name is 'Тип топлива (92 бензин, 95 бензин, дизель)';

-- auto_car_states --
CREATE TABLE IF NOT EXISTS auto_car_states
(
  acs_id   serial PRIMARY KEY,
  acs_name character varying(25)
);

comment on table  auto_car_states          is 'Статусы автомобилей автопарка';
comment on column auto_car_states.acs_id   is 'Код статуса (ключ)';
comment on column auto_car_states.acs_name is 'Статус (готов к рейсу, неисправен, ТО)';

-- auto_car_types --
CREATE TABLE IF NOT EXISTS auto_car_types
(
  act_id   serial PRIMARY KEY,
  act_name character varying(25)
);

comment on table  auto_car_types          is 'Типы автомобилей автопарка';
comment on column auto_car_types.act_id   is 'Код типа (ключ)';
comment on column auto_car_types.act_name is 'Тип (легковой, грузовой)';

-- auto_device_types --
CREATE TABLE IF NOT EXISTS auto_device_types
(
  adt_id          serial PRIMARY KEY,
  adt_config      text,
  adt_config_time timestamp without time zone,
  adt_name        character varying(100)
);

comment on table  auto_device_types                 is 'Типы устройств автопарка с настройками';
comment on column auto_device_types.adt_id          is 'Код типа (ключ)';
comment on column auto_device_types.adt_config      is 'Настройки устройств этого типа';
comment on column auto_device_types.adt_config_time is 'Время изменения конфигурации';
comment on column auto_device_types.adt_name        is 'Название типа';

-- auto_devices --
CREATE TABLE IF NOT EXISTS auto_devices
(
  adev_id          serial PRIMARY KEY,
  adev_code        numeric(19,2),
  adev_last_config timestamp without time zone,
  adev_model       character varying(30),
  adev_type        integer REFERENCES auto_device_types (adt_id) ON UPDATE CASCADE ON DELETE RESTRICT
);

comment on table  auto_devices                  is 'Устройства-трекеры для контроля автомобилей';
comment on column auto_devices.adev_id          is 'Код устройства во внешней системе (ключ)';
comment on column auto_devices.adev_code        is 'Код устройства (IMEI для телефона)';
comment on column auto_devices.adev_last_config is 'Время обновления конфигурации на устройстве';
comment on column auto_devices.adev_model       is 'Модель устройства';
comment on column auto_devices.adev_type        is 'Тип устройства (ссылка на auto_device_types)';

-- auto_driver_categories --
CREATE TABLE IF NOT EXISTS auto_driver_categories
(
  adc_id   serial PRIMARY KEY,
  adc_name character varying(3)
);

comment on table  auto_driver_categories          is 'Категории водителей автопарка';
comment on column auto_driver_categories.adc_id   is 'Код категории (ключ)';
comment on column auto_driver_categories.adc_name is 'Название категории (B, C)';

-- auto_cars --
CREATE TABLE IF NOT EXISTS auto_cars
(
  ac_id           serial PRIMARY KEY,
  ac_capacity     real,
  ac_inspect_date date,
  ac_license      character varying(12),
  ac_manufactured date,
  ac_mileage      bigint,
  ac_model        character varying(50),
  as_purchased    date,
  ac_device       integer REFERENCES auto_devices (adev_id)          ON UPDATE CASCADE ON DELETE SET NULL,
  ac_driver_class integer REFERENCES auto_driver_categories (adc_id) ON UPDATE CASCADE ON DELETE SET NULL,
  ac_fuel         integer REFERENCES auto_car_fuel (acf_id)          ON UPDATE CASCADE ON DELETE SET NULL,
  ac_state        integer REFERENCES auto_car_states (acs_id)        ON UPDATE CASCADE ON DELETE SET NULL,
  ac_type         integer REFERENCES auto_car_types (act_id)         ON UPDATE CASCADE ON DELETE SET NULL
);

comment on table  auto_cars                 is 'Автомобили автопарка';
comment on column auto_cars.ac_id           is 'Код автомобиля (ключ)';
comment on column auto_cars.ac_capacity     is 'Грузоподъёмность';
comment on column auto_cars.ac_inspect_date is 'Дата предыдущего ТО';
comment on column auto_cars.ac_license      is 'Гос. номер';
comment on column auto_cars.ac_manufactured is 'Год выпуска';
comment on column auto_cars.ac_mileage      is 'Показания одометра';
comment on column auto_cars.as_purchased    is 'Дата приобретения';
comment on column auto_cars.ac_model        is 'Марка';
comment on column auto_cars.ac_device       is 'Привязанное устройство по-умолчанию (ссылка на auto_devices)';
comment on column auto_cars.ac_driver_class is 'Требуемый класс водителя (ссылка на auto_driver_class)';
comment on column auto_cars.ac_fuel         is 'Тип топлива (ссылка на auto_car_fuel)';
comment on column auto_cars.ac_state        is 'Состояние автомобиля (ссылка на auto_car_states)';
comment on column auto_cars.ac_type         is 'Тип автомобиля (ссылка на auto_car_types)';

-- auto_drivers --
CREATE TABLE IF NOT EXISTS auto_drivers
(
  ad_id           serial PRIMARY KEY,
  ad_license_date date,
  ad_license_num  character varying(8),
  ad_license_ser  character varying(6),
  ad_name         character varying(100),
  ad_car          integer REFERENCES auto_cars (ac_id) ON UPDATE CASCADE ON DELETE SET NULL,
  ad_category     integer REFERENCES auto_driver_categories (adc_id) ON UPDATE CASCADE ON DELETE SET NULL
);

comment on table  auto_drivers                 is 'Справочник водителей автопарка';
comment on column auto_drivers.ad_id           is 'Код водителя (ключ)';
comment on column auto_drivers.ad_license_date is 'Дата выдачи В/У';
comment on column auto_drivers.ad_license_num  is 'Номер В/У';
comment on column auto_drivers.ad_license_ser  is 'Серия В/У';
comment on column auto_drivers.ad_name         is 'Имя и фамилия водителя';
comment on column auto_drivers.ad_car          is 'Привязанный по-умолчанию автомобиль (ссылка на auto_driver_cars)';
comment on column auto_drivers.ad_category     is 'Класс водителя (ссылка на auto_driver_сategory)';

-- auto_places --
CREATE TABLE IF NOT EXISTS auto_places
(
  aplace_id   serial PRIMARY KEY,
  aplace_name character varying(50)
);

comment on table  auto_places             is 'Места подачи автомобилей';
comment on column auto_places.aplace_id   is 'Код места подачи (ключ)';
comment on column auto_places.aplace_name is 'Описание места подачи';

-- auto_points --
CREATE TABLE IF NOT EXISTS auto_points
(
  apoint_id      serial PRIMARY KEY,
  apoint_lat     real,
  apoint_lon     real,
  apoint_height  real,
  apoint_dir     integer,
  apoint_dist    bigint,
  apoint_speed   real,
  apoint_charge  integer,
  apoint_systime timestamp without time zone,
  apoint_time    timestamp without time zone,
  apoint_device  integer REFERENCES auto_devices (adev_id) ON UPDATE CASCADE ON DELETE SET NULL
);

comment on table  auto_points                is 'Координаты устройств автопарка';
comment on column auto_points.apoint_id      is 'Код точки (ключ)';
comment on column auto_points.apoint_lat     is 'Широта';
comment on column auto_points.apoint_lon     is 'Долгота';
comment on column auto_points.apoint_height  is 'Высота';
comment on column auto_points.apoint_dir     is 'Направление';
comment on column auto_points.apoint_dist    is 'Расстояние с начала измерения';
comment on column auto_points.apoint_speed   is 'Скорость';
comment on column auto_points.apoint_charge  is 'Заряд батареи';
comment on column auto_points.apoint_time    is 'Дата и время с устройства';
comment on column auto_points.apoint_systime is 'время записи';
comment on column auto_points.apoint_device  is 'Код устройства (ссылка на auto_devices)';

-- auto_routes --
CREATE TABLE IF NOT EXISTS auto_routes
(
  aroute_id   serial PRIMARY KEY,
  aroute_name character varying(50)
);

comment on table  auto_routes             is 'Маршруты автопарка';
comment on column auto_routes.aroute_id   is 'Код маршрута (ключ)';
comment on column auto_routes.aroute_name is 'Название маршрута';

-- auto_zones --
CREATE TABLE IF NOT EXISTS auto_zones
(
  az_id      serial PRIMARY KEY,
  az_polygon geometry,
  az_route   integer REFERENCES public.auto_routes (aroute_id) ON UPDATE CASCADE ON DELETE SET NULL
);

comment on table  auto_zones            is 'Зоны автопарка';
comment on column auto_zones.az_id      is 'Код зоны (ключ)';
comment on column auto_zones.az_polygon is 'Многоугольник, описывающий зону';
comment on column auto_zones.az_route   is 'Маршрут, для которого предназначена зона';

-- auto_races --
CREATE TABLE IF NOT EXISTS auto_races
(
  arace_id        serial PRIMARY KEY,
  arace_closetime timestamp without time zone,
  arace_execdate  date NOT NULL,
  arace_opentime  timestamp without time zone,
  arace_tasktxt   character varying(2000),
  arace_car       integer,
  arace_device    integer REFERENCES auto_devices (adev_id)  ON UPDATE CASCADE ON DELETE SET NULL,
  arace_driver    integer REFERENCES auto_drivers (ad_id)    ON UPDATE CASCADE ON DELETE SET NULL,
  arace_place     integer REFERENCES auto_places (aplace_id) ON UPDATE CASCADE ON DELETE SET NULL,
  arace_route     integer REFERENCES auto_routes (aroute_id) ON UPDATE CASCADE ON DELETE SET NULL,
  arace_zone      integer REFERENCES auto_zones (az_id)      ON UPDATE CASCADE ON DELETE SET NULL,
  arace_corr      double precision DEFAULT 0,
  arace_corrcause character varying(500)
);

comment on table  auto_races                 is 'Рейсы автопарка';
comment on column auto_races.arace_id        is 'Код рейса (ключ)';
comment on column auto_races.arace_closetime is 'Время закрытия';
comment on column auto_races.arace_execdate  is 'Дата выполнения';
comment on column auto_races.arace_opentime  is 'Время открытия';
comment on column auto_races.arace_tasktxt   is 'Задания водителю в текстовом виде';
comment on column auto_races.arace_car       is 'Автомобиль (ссылка на auto_cars)';
comment on column auto_races.arace_device    is 'Устройство-трекер (ссылка на auto_devices)';
comment on column auto_races.arace_driver    is 'Водитель (ссылка на auto_drivers)';
comment on column auto_races.arace_place     is 'Место подачи (ссылка на auto_places)';
comment on column auto_races.arace_route     is 'Маршрут (ссылка на auto_route)';
comment on column auto_races.arace_zone      is 'Зона, по которой прохоидит рейс (ссылка на auto_zones)';

COMMIT;
